/*
 * main.c
 *
 *  Created on: Aug 16, 2021
 *      Author: Admin
 */


#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "stdint.h"

spkf_data  data;
void show_matrix(float *A, int n) {
	int i, j;
    for ( i = 0; i < n; i++) {
        for ( j = 0; j < n; j++)
            printf("%2.5f ", A[i * n + j]);
        printf("\n");
    }
}

int main()
{
    int n = 3;
    float m1[3][3] ={ {25, 15, -5},
    		         {  15, 18,  0},
					 {-5,  0, 11}};
    float *c1 = cholesky(n,m1);
    show_matrix(c1, n);
    printf("\n");
    spkf_soc_init(&data);
    spkf_soc_iter(&data);
	return 0;
}
